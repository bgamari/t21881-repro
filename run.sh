$GHC \
	-fforce-recomp \
	T21881.hs T21881_cpp.cpp \
	-hide-all-packages \
	-package base \
	-package system-cxx-std-lib \
	-o T21881.exe \
	$@ && \
	./T21881.exe
