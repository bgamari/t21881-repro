#include <Rts.h>

extern void hello(void);
extern StgClosure ZCMain_main_closure;

int main(int argc, char **argv) {
    RtsConfig __conf = defaultRtsConfig;
    __conf.rts_opts_enabled = RtsOptsSafeOnly;
    __conf.rts_opts_suggestions = true;
    __conf.keep_cafs = false;
    __conf.rts_hs_main = true;

    hs_init_ghc(&argc, &argv, __conf);

    __register_hs_exception_handler();

    // This fails
    {
        Capability *cap = rts_lock();
        rts_evalLazyIO(&cap, &ZCMain_main_closure, NULL);
        rts_unlock(cap);
    }

    // Whereas this works
    hello_c();

    return 0;
}
