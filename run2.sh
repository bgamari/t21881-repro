set -ex
root=$HOME/ghc/_build
ghc=$root/stage1/bin/ghc
mingw=$root/mingw
clang=$mingw/bin/clang

lib_dirs=(
    "-L$root/stage1/lib" "-L$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/base-4.17.0.0" "-L$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/ghc-bignum-1.3" "-L$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/ghc-prim-0.9.0" "-L$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/rts-1.0.2"
)
rts_args=(
    "-Wl,-u,hs_atomic_add64" "-Wl,-u,hs_atomic_sub64" "-Wl,-u,hs_atomic_and64" "-Wl,-u,hs_atomic_nand64" "-Wl,-u,hs_atomic_or64" "-Wl,-u,hs_atomic_xor64" "-Wl,-u,hs_atomicread64" "-Wl,-u,hs_atomicwrite64" "-Wl,-u,base_GHCziTopHandler_runIO_closure" "-Wl,-u,base_GHCziTopHandler_runNonIO_closure" "-Wl,-u,ghczmprim_GHCziTuple_Z0T_closure" "-Wl,-u,ghczmprim_GHCziTypes_True_closure" "-Wl,-u,ghczmprim_GHCziTypes_False_closure" "-Wl,-u,base_GHCziPack_unpackCString_closure" "-Wl,-u,base_GHCziWeakziFinalizze_runFinalizzerBatch_closure" "-Wl,-u,base_GHCziIOziException_stackOverflow_closure" "-Wl,-u,base_GHCziIOziException_heapOverflow_closure" "-Wl,-u,base_GHCziIOziException_allocationLimitExceeded_closure" "-Wl,-u,base_GHCziIOziException_blockedIndefinitelyOnMVar_closure" "-Wl,-u,base_GHCziIOziException_blockedIndefinitelyOnSTM_closure" "-Wl,-u,base_GHCziIOziException_cannotCompactFunction_closure" "-Wl,-u,base_GHCziIOziException_cannotCompactPinned_closure" "-Wl,-u,base_GHCziIOziException_cannotCompactMutable_closure" "-Wl,-u,base_GHCziIOPort_doubleReadException_closure" "-Wl,-u,base_ControlziExceptionziBase_nonTermination_closure" "-Wl,-u,base_ControlziExceptionziBase_nestedAtomically_closure" "-Wl,-u,base_GHCziEventziThread_blockedOnBadFD_closure" "-Wl,-u,base_GHCziConcziSync_runSparks_closure" "-Wl,-u,base_GHCziConcziIO_ensureIOManagerIsRunning_closure" "-Wl,-u,base_GHCziConcziIO_interruptIOManager_closure" "-Wl,-u,base_GHCziConcziIO_ioManagerCapabilitiesChanged_closure" "-Wl,-u,base_GHCziConcziSignal_runHandlersPtr_closure" "-Wl,-u,base_GHCziTopHandler_flushStdHandles_closure" "-Wl,-u,base_GHCziTopHandler_runMainIO_closure" "-Wl,-u,ghczmprim_GHCziTypes_Czh_con_info" "-Wl,-u,ghczmprim_GHCziTypes_Izh_con_info" "-Wl,-u,ghczmprim_GHCziTypes_Fzh_con_info" "-Wl,-u,ghczmprim_GHCziTypes_Dzh_con_info" "-Wl,-u,ghczmprim_GHCziTypes_Wzh_con_info" "-Wl,-u,base_GHCziPtr_Ptr_con_info" "-Wl,-u,base_GHCziPtr_FunPtr_con_info" "-Wl,-u,base_GHCziInt_I8zh_con_info" "-Wl,-u,base_GHCziInt_I16zh_con_info" "-Wl,-u,base_GHCziInt_I32zh_con_info" "-Wl,-u,base_GHCziInt_I64zh_con_info" "-Wl,-u,base_GHCziWord_W8zh_con_info" "-Wl,-u,base_GHCziWord_W16zh_con_info" "-Wl,-u,base_GHCziWord_W32zh_con_info" "-Wl,-u,base_GHCziWord_W64zh_con_info" "-Wl,-u,base_GHCziStable_StablePtr_con_info" "-Wl,-u,hs_atomic_add8" "-Wl,-u,hs_atomic_add16" "-Wl,-u,hs_atomic_add32" "-Wl,-u,hs_atomic_sub8" "-Wl,-u,hs_atomic_sub16" "-Wl,-u,hs_atomic_sub32" "-Wl,-u,hs_atomic_and8" "-Wl,-u,hs_atomic_and16" "-Wl,-u,hs_atomic_and32" "-Wl,-u,hs_atomic_nand8" "-Wl,-u,hs_atomic_nand16" "-Wl,-u,hs_atomic_nand32" "-Wl,-u,hs_atomic_or8" "-Wl,-u,hs_atomic_or16" "-Wl,-u,hs_atomic_or32" "-Wl,-u,hs_atomic_xor8" "-Wl,-u,hs_atomic_xor16" "-Wl,-u,hs_atomic_xor32" "-Wl,-u,hs_cmpxchg8" "-Wl,-u,hs_cmpxchg16" "-Wl,-u,hs_cmpxchg32" "-Wl,-u,hs_cmpxchg64" "-Wl,-u,hs_xchg8" "-Wl,-u,hs_xchg16" "-Wl,-u,hs_xchg32" "-Wl,-u,hs_xchg64" "-Wl,-u,hs_atomicread8" "-Wl,-u,hs_atomicread16" "-Wl,-u,hs_atomicread32" "-Wl,-u,hs_atomicwrite8" "-Wl,-u,hs_atomicwrite16" "-Wl,-u,hs_atomicwrite32" "-Wl,-u,base_GHCziStackziCloneStack_StackSnapshot_closure" "-Wl,-u,base_GHCziEventziWindows_processRemoteCompletion_closure"
)
libs=(
    "-lHSbase-4.17.0.0" "-lHSghc-bignum-1.3" "-lHSghc-prim-0.9.0" "-lHSrts-1.0.2" "-lCffi-6" "-l:libc++.a" "-l:libc++abi.a" "-lwsock32" "-luser32" "-lshell32" "-lmingw32" "-lkernel32" "-ladvapi32" "-lmingwex" "-lws2_32" "-lshlwapi" "-lole32" "-lrpcrt4" "-lntdll" "-luser32" "-lmingw32" "-lmingwex" "-lucrt" "-lm" "-lwsock32" "-lgdi32" "-lwinmm" "-ldbghelp" "-lpsapi" "-lpthread" "-lmingwex"
)

run() {
    $ghc -c T21881.hs $@
    $clang -c startup.c  "-I$root\stage1\lib\x86_64-windows-ghc-9.5.20220718\rts-1.0.2\include"
    $clang++ -I$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/rts-1.0.2/include -c T21881_cpp.cpp
    $clang   -I$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/rts-1.0.2/include -c T21881_c.c
    $clang   -I$root/stage1/lib/x86_64-windows-ghc-9.5.20220718/rts-1.0.2/include -c main.c
    $clang \
        -o T21881.exe \
        T21881.o \
        T21881_c.o \
        T21881_cpp.o \
        main.o \
        ${lib_dirs[@]} \
        ${rts_args[@]} \
        ${libs[@]}
    
    ./T21881.exe
}

echo bad; run -DCALL_CXX
echo bad; run
