{-# LANGUAGE CPP #-}
{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign.C.Types

#if defined(CALL_CXX)
foreign import ccall unsafe  "hello_cpp" hello :: IO CInt
#else
foreign import ccall unsafe  "hello_c" hello :: IO CInt
#endif

main :: IO ()
main = hello >>= print
