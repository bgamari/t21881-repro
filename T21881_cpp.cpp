#include <stdexcept>
#include <iostream>

extern "C" {
int hello_cpp() {
	try {
		throw std::runtime_error("hello world");
	} catch (const std::exception &e) {
		std::cout << "exception " << "\n";
	}
	return 52;
}
}
